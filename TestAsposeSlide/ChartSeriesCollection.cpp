
#include "ChartSeriesCollection.h"

CIChartSeriesCollection::CIChartSeriesCollection(System::SharedPtr<Aspose::Slides::Charts::IChartSeriesCollection> ChartSeriesCollection)
{
	m_ChartSeriesCollection = ChartSeriesCollection;
}

bool CIChartSeriesCollection::IsValid() const
{
	return m_ChartSeriesCollection != nullptr;
}