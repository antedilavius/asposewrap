#pragma once

#include "asposeDef.h"
#include <DOM/Chart/Chart.h>

class CShape;
class CIChartData;

enum ChartTypesWrap
{
	Bar,
	Line,
	Pie,
	NoSupport
};

class EXPORT_DLL_API CChart
{
public:
	CChart() = default;
	CChart(System::SharedPtr<Aspose::Slides::Charts::Chart> chart);
	CChart(CShape const& shape);
	bool IsValid() const;
	ChartTypesWrap GetType() const;
	CIChartData GenChartData();
	
	bool HasTitle() const;
	std::wstring const& GetTitle() const;
	std::wstring const& GetCategoryName(int index) const;

private:
	System::SharedPtr<Aspose::Slides::Charts::Chart> m_Chart;
};