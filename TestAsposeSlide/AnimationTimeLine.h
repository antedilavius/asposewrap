#pragma once

#include "asposeDef.h"
#include <DOM/IAnimationTimeLine.h>

class ISequence;

class EXPORT_DLL_API AnimationTimeLine
{
public:
	AnimationTimeLine() = default;
	AnimationTimeLine(System::SharedPtr<Aspose::Slides::IAnimationTimeLine> timeLine);

	ISequence GetMainSequence();
	bool IsValid() const;

private:
	System::SharedPtr<Aspose::Slides::IAnimationTimeLine> m_TimeLine;
};