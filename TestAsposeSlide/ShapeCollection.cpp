
#include "ShapeCollection.h"
#include "ShapeWrap.h"

CIShapeCollection::CIShapeCollection(System::SharedPtr<Aspose::Slides::IShapeCollection> shapeCollection)
{
	m_ShapeCollection = shapeCollection;
}

int32_t CIShapeCollection::GetCount()
{
	return m_ShapeCollection->get_Count();
}

CShape CIShapeCollection::IdxGet(int32_t index)
{
	return CShape(m_ShapeCollection->idx_get(index));
}

bool CIShapeCollection::IsValid() const
{
	return m_ShapeCollection != nullptr;
}