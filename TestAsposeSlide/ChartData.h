#pragma once

#include "asposeDef.h"
#include "DOM/Chart/ChartData.h"

class CIChartSeriesCollection;

class EXPORT_DLL_API CIChartData
{
public:
	CIChartData() = default;
	CIChartData(System::SharedPtr<Aspose::Slides::Charts::IChartData> chartData);

	bool IsValid() const;
	CIChartSeriesCollection GetChartSeriesCollection();

private:
	System::SharedPtr<Aspose::Slides::Charts::IChartData> m_ChartData;
};
