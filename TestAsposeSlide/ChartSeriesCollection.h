#pragma once

#include "asposeDef.h"
#include <DOM/Chart/ChartSeriesCollection.h>

class EXPORT_DLL_API CIChartSeriesCollection
{
public:
	CIChartSeriesCollection() = default;
	CIChartSeriesCollection(System::SharedPtr<Aspose::Slides::Charts::IChartSeriesCollection> ChartSeriesCollection);
	bool IsValid() const;

public:
	System::SharedPtr<Aspose::Slides::Charts::IChartSeriesCollection> m_ChartSeriesCollection;
};