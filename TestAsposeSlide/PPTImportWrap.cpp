#include "PPTImportWrap.h"
#include <DOM/Presentation.h>
#include <Util/License.h>

void CPPTImport::SetLicense(std::wstring const& licencePath)
{
	license = new Aspose::Slides::License();
	System::String pathLicence(licencePath);
	license->SetLicense(pathLicence);
}

void CPPTImport::MakePresentation(std::wstring const& pptPath)
{
	System::String pathPresentation(pptPath);
	m_Presentation = CPresentation(System::MakeObject<Aspose::Slides::Presentation>(pathPresentation));
}