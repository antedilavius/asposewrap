#pragma once

#include "asposeDef.h"
#include <DOM/Presentation.h>


class CSlide;

class EXPORT_DLL_API CPresentation
{
public:
	CPresentation() = default;
	CPresentation(System::SharedPtr<Aspose::Slides::Presentation> presentation);

	CSlide GetSlide(int32_t SlideID);
	uint32_t GetCount() const;

	void ExtractSlideImage(int32_t SlideID, std::wstring const& imagePathManaged, float scaleFactor);

	bool IsValid() const;

	float get_SlideWidth();
	float get_SlideHeight();


private:
	System::SharedPtr<Aspose::Slides::Presentation> m_Presentation;
};