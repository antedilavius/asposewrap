
#include "ChartWrap.h"
#include "ShapeWrap.h"

#include "ChartData.h"
#include <DOM/TextFrame.h>
#include <DOM/Chart/ChartTitle.h>
#include <DOM/Chart/ChartType.h>
#include "DOM/Chart/ChartCategoryCollection.h"
#include <DOM/Chart/IChartData.h>
#include <DOM/Chart/ChartDataCell.h>

CChart::CChart(System::SharedPtr<Aspose::Slides::Charts::Chart> chart)
{
	m_Chart = chart;
}

CChart::CChart(CShape const& shape)
{
	m_Chart = System::dynamic_pointer_cast<Aspose::Slides::Charts::Chart>(shape.m_Shape );
}

bool CChart::IsValid() const
{
	return m_Chart != nullptr;
}

ChartTypesWrap CChart::GetType() const
{
	ChartTypesWrap chartType(ChartTypesWrap::NoSupport);

	switch( m_Chart->get_Type() ) {
	case Aspose::Slides::Charts::ChartType::ClusteredBar:
	case Aspose::Slides::Charts::ChartType::StackedBar:
	case Aspose::Slides::Charts::ChartType::ClusteredBar3D:
	case Aspose::Slides::Charts::ChartType::ClusteredCylinder:
	case Aspose::Slides::Charts::ChartType::StackedBar3D:
	case Aspose::Slides::Charts::ChartType::ClusteredColumn:
	case Aspose::Slides::Charts::ChartType::StackedColumn:
	case Aspose::Slides::Charts::ChartType::ClusteredColumn3D:
	case Aspose::Slides::Charts::ChartType::StackedColumn3D:
	case Aspose::Slides::Charts::ChartType::Column3D:
		chartType = ChartTypesWrap::Bar;
		break;
	case Aspose::Slides::Charts::ChartType::Line:
	case Aspose::Slides::Charts::ChartType::StackedLine:
	case Aspose::Slides::Charts::ChartType::StackedLineWithMarkers:
	case Aspose::Slides::Charts::ChartType::LineWithMarkers:
	case Aspose::Slides::Charts::ChartType::Line3D:
		chartType = ChartTypesWrap::Line;
		break;
	case Aspose::Slides::Charts::ChartType::Pie:
	case Aspose::Slides::Charts::ChartType::Pie3D:
	case Aspose::Slides::Charts::ChartType::PieOfPie:
	case Aspose::Slides::Charts::ChartType::BarOfPie:
	case Aspose::Slides::Charts::ChartType::Doughnut:
	case Aspose::Slides::Charts::ChartType::ExplodedPie3D:
	case Aspose::Slides::Charts::ChartType::ExplodedPie:
		chartType = ChartTypesWrap::Pie;
		break;
	}
	return chartType;
}

CIChartData CChart::GenChartData()
{
	return CIChartData(m_Chart->get_ChartData());
}

bool CChart::HasTitle() const
{
	return m_Chart->get_HasTitle();
}

std::wstring const& CChart::GetTitle() const
{
	static std::wstring title = L"";

	title = L"";

	auto chartTitle = m_Chart->get_ChartTitle();
	if( chartTitle ) {
		auto textFrame = chartTitle->get_TextFrameForOverriding();
		if( textFrame ) {
			title = textFrame->get_Text().ToWCS();
		}
	}
	return title;
}

std::wstring const& CChart::GetCategoryName(int index) const
{
	static std::wstring categoryName;
	auto category = m_Chart->get_ChartData()->get_Categories();

	categoryName = category->idx_get(index)->get_AsCell()->get_Value()->ToString().ToWCS();

	return categoryName;
}