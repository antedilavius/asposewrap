
#include "PresentationWrap.h"
#include <DOM/SlideCollection.h>
#include <DOM/SlideSize.h>
#include "SlideWrap.h"
#include "Bitmap.h"

CPresentation::CPresentation(System::SharedPtr<Aspose::Slides::Presentation> presentation)
{
	m_Presentation = presentation;
}

float CPresentation::get_SlideWidth()
{
	return m_Presentation->get_SlideSize()->get_Size().get_Width();
}

float CPresentation::get_SlideHeight()
{
	return m_Presentation->get_SlideSize()->get_Size().get_Height();
}

CSlide CPresentation::GetSlide(int32_t SlideID)
{
	return CSlide( m_Presentation->get_Slides()->idx_get(SlideID));
}

uint32_t  CPresentation::GetCount() const
{
	return m_Presentation->get_Slides()->get_Count();
}

void CPresentation::ExtractSlideImage(int32_t SlideID, std::wstring const& imagePathManaged, float scaleFactor)
{
	auto slide = GetSlide(SlideID);
	if( slide.IsValid() ) {
		auto image = slide.GetBitmap(scaleFactor, scaleFactor);

		image.Save(imagePathManaged);
		int32_t tt = 0;
	}
}

bool CPresentation::IsValid() const
{
	return m_Presentation != nullptr;
}

