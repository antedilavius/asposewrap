#pragma once

#include "asposeDef.h"
#include <DOM/Shape.h>

class EXPORT_DLL_API CShape
{
	friend class ISequence;
public:
	CShape() = default;
	CShape(System::SharedPtr<Aspose::Slides::IShape> Shape);

	void SetHidden(bool value);
	bool IsValid() const;

public:
	System::SharedPtr<Aspose::Slides::IShape> m_Shape;
};