#pragma once

#include "asposeDef.h"
#include <DOM/TextFrame.h>

class EXPORT_DLL_API CTextFrame
{
public:
	CTextFrame() = default;
	CTextFrame(System::SharedPtr<Aspose::Slides::ITextFrame> textFrame);

	bool IsValid() const;
		
	std::wstring GetText();

private:
	System::SharedPtr < Aspose::Slides::ITextFrame> m_TextFrame;
};
