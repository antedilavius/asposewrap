#pragma once

#include "asposeDef.h"
#include "PresentationWrap.h"

namespace Aspose {
namespace Slides {
	class License;
}
}

class EXPORT_DLL_API CPPTImport
{
public:
	CPPTImport() = default;

	void SetLicense(std::wstring const& licencePath);
	void MakePresentation(std::wstring const& pptPath);

public:
	CPresentation m_Presentation;

	System::SharedPtr<Aspose::Slides::License> license;
};