
#include "Bitmap.h"

CBitmap::CBitmap(System::SharedPtr<System::Drawing::Bitmap> image)
{
	m_Image = image;
}

void CBitmap::Save(std::wstring const& imagePath)
{
	m_Image->Save(System::String(imagePath.c_str()));
}