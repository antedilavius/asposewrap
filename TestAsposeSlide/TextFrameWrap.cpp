
#include "TextFrameWrap.h"

CTextFrame::CTextFrame(System::SharedPtr<Aspose::Slides::ITextFrame> textFrame)
{
	m_TextFrame = textFrame;
}

bool CTextFrame::IsValid() const
{
	return m_TextFrame != nullptr;
}

std::wstring CTextFrame::GetText()
{
	return m_TextFrame->get_Text().ToWCS();
}

