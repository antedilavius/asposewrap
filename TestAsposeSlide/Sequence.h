#pragma once

#include "asposeDef.h"
#include <DOM/Animation/ISequence.h>

class CShape;

class EXPORT_DLL_API ISequence
{
public:
	ISequence() = default;
	ISequence(System::SharedPtr<Aspose::Slides::Animation::ISequence> sequence);
	int32_t GetCount(CShape& shape);
	bool IsValid() const;
private:

	System::SharedPtr<Aspose::Slides::Animation::ISequence> m_Sequence;
};
