
#include "ShapeWrap.h"
#include <DOM/Chart/Chart.h>
#include <DOM/Chart/ChartData.h>
#include <DOM/Chart/ChartDataCell.h>

CShape::CShape(System::SharedPtr<Aspose::Slides::IShape> Shape)
{
	m_Shape = Shape;
}

void CShape::SetHidden(bool value)
{
	m_Shape->set_Hidden(value);
}

bool CShape::IsValid() const
{
	return m_Shape != nullptr;
}