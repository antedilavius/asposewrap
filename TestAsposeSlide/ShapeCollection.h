#pragma once

#include "asposeDef.h"
#include <DOM/IShapeCollection.h>

class CShape;

class EXPORT_DLL_API CIShapeCollection
{
public:
	CIShapeCollection() = default;
	CIShapeCollection(System::SharedPtr<Aspose::Slides::IShapeCollection> shapeCollection);

	int32_t GetCount();
	CShape IdxGet(int32_t index);
	bool IsValid() const;

private:
	System::SharedPtr<Aspose::Slides::IShapeCollection> m_ShapeCollection;
};