#pragma once

#include "asposeDef.h"
#include <DOM/Chart/ChartSeriesCollection.h>

class EXPORT_DLL_API CChartSeries
{
public:
	CChartSeries() = default;
	CChartSeries(System::SharedPtr<Aspose::Slides::Charts::IChartSeries> ChartSeries);

	std::vector<int> GetColor(int index);
	bool HasValue(int index);
	double GetValue(int index);
	std::string const& GetSeriesName();
	int GetNumberOfElements();
	bool IsValid() const;

public:
	System::SharedPtr<Aspose::Slides::Charts::IChartSeries> m_ChartSeries;
	std::string name;
};