
#include "ChartData.h"
#include "ChartSeriesCollection.h"

CIChartData::CIChartData(System::SharedPtr<Aspose::Slides::Charts::IChartData> chartData)
{
	m_ChartData = chartData;
}

bool CIChartData::IsValid() const
{
	return m_ChartData != nullptr;
}

CIChartSeriesCollection CIChartData::GetChartSeriesCollection()
{
	return CIChartSeriesCollection(m_ChartData->get_Series());
}