#pragma once

#include "asposeDef.h"
#include <DOM/NotesSlide.h>
#include "TextFrameWrap.h"

class EXPORT_DLL_API CNote
{
public:
	CNote() = default;
	CNote(System::SharedPtr<Aspose::Slides::INotesSlide> node);

	CTextFrame GetTextFrame();
	bool IsValid() const;

private:
	System::SharedPtr<Aspose::Slides::INotesSlide> m_Note;
};