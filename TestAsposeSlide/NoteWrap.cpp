
#include "NoteWrap.h"

CNote::CNote(System::SharedPtr<Aspose::Slides::INotesSlide> node)
{
	m_Note = node;
}

CTextFrame CNote::GetTextFrame()
{
	return CTextFrame(m_Note->get_NotesTextFrame());
}

bool CNote::IsValid() const
{
	return m_Note != nullptr;
}