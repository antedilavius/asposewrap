#pragma once

#include "asposeDef.h"
#include <DOM/Slide.h>
#include <string>

class CShape;
class CNote;
class CIShapeCollection;
class CBitmap;

class EXPORT_DLL_API CSlide
{
	friend class CChart;
public:
	CSlide() = default;
	CSlide(System::SharedPtr<Aspose::Slides::ISlide> Slide);

	std::wstring const& ExtractSlideNote() const;

	CIShapeCollection GetShapeCollection() const;

	CNote GetNote() const;
	CShape AsShape();

	bool IsValid() const;
	CBitmap GetBitmap(float scaleX, float scaleY) const;

public:
	System::SharedPtr<Aspose::Slides::ISlide> m_Slide;
};