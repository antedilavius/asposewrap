
#include "AnimationTimeLine.h"
#include "Sequence.h"

AnimationTimeLine::AnimationTimeLine(System::SharedPtr<Aspose::Slides::IAnimationTimeLine> timeLine)
{
	m_TimeLine = timeLine;
}

ISequence AnimationTimeLine::GetMainSequence()
{
	return ISequence(m_TimeLine->get_MainSequence());
}

bool AnimationTimeLine::IsValid() const
{
	return m_TimeLine != nullptr;
}