
#include "Sequence.h"
#include "ShapeWrap.h"

ISequence::ISequence(System::SharedPtr<Aspose::Slides::Animation::ISequence> sequence)
{
	m_Sequence = sequence;
}

int32_t ISequence::GetCount(CShape& shape)
{
	return m_Sequence->GetCount(System::dynamic_pointer_cast<Aspose::Slides::Shape>( shape.m_Shape));
}

bool ISequence::IsValid() const
{
	return m_Sequence != nullptr;
}