#pragma once

#include "asposeDef.h"
#include <drawing/bitmap.h>

class EXPORT_DLL_API CBitmap
{
public:
	CBitmap() = default;
	CBitmap(System::SharedPtr<System::Drawing::Bitmap> image);

	void Save(std::wstring const& imagePath);

private:
	System::SharedPtr<System::Drawing::Bitmap> m_Image;
};
