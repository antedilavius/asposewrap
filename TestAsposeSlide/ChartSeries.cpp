
#include "ChartSeries.h"
#include <DOM/Chart/Chart.h>
#include <DOM/Chart/ChartData.h>
#include <DOM/Chart/ChartDataCell.h>
#include <DOM/Chart/ChartSeries.h>
#include <DOM/Chart/ChartCategoryCollection.h>
#include <DOM/Chart/ChartDataPointCollection.h>
#include <DOM/Chart/ChartDataPoint.h>
#include <DOM/Chart/IDoubleChartValue.h>
#include <DOM/Chart/IStringChartValue.h>

CChartSeries::CChartSeries(System::SharedPtr<Aspose::Slides::Charts::IChartSeries> ChartSeries)
{
	m_ChartSeries = ChartSeries;
}

std::vector<int> CChartSeries::GetColor(int index)
{
	std::vector<int> res;
	System::Drawing::Color seriesColor = System::Drawing::Color::get_Black();

	seriesColor = m_ChartSeries->get_DataPoints()->idx_get(index)->GetAutomaticDataPointColor();

	res.push_back(seriesColor.get_R());
	res.push_back(seriesColor.get_G());
	res.push_back(seriesColor.get_B());

	return res;
}

bool CChartSeries::HasValue(int index)
{
	return m_ChartSeries->get_DataPoints()->GetOrCreateDataPointByIdx(index) != nullptr;
}

double CChartSeries::GetValue(int index)
{
	return m_ChartSeries->get_DataPoints()->idx_get(index)->get_Value()->ToDouble();
}

std::string const& CChartSeries::GetSeriesName()
{
	System::String NameManaged = m_ChartSeries->get_Name()->ToString();
	name = NameManaged.ToUtf8String();

	return name;
}

int CChartSeries::GetNumberOfElements()
{
	return m_ChartSeries->get_DataPoints()->get_Count();
}

bool CChartSeries::IsValid() const
{
	return m_ChartSeries != nullptr;
}