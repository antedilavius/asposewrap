
#include "SlideWrap.h"
#include <DOM/SlideCollection.h>
#include <DOM/ShapeCollection.h>
#include <DOM/NotesSlideManager.h>
#include <DOM/NotesSlide.h>

#include "ShapeWrap.h"
#include "NoteWrap.h"
#include "ShapeCollection.h"
#include "Bitmap.h"


CSlide::CSlide(System::SharedPtr<Aspose::Slides::ISlide> Slide)
{
	m_Slide = Slide;
}

std::wstring const& CSlide::ExtractSlideNote() const
{
	static std::wstring m_TextFrameText;

	auto note = GetNote();
	if ( note.IsValid() )
	{
		auto textFrame = note.GetTextFrame();
		if( textFrame.IsValid() ) {
			m_TextFrameText = textFrame.GetText();
		}
	} else {
		m_TextFrameText = (L"nullptr error");
	}

	return m_TextFrameText;
}

CIShapeCollection CSlide::GetShapeCollection() const
{
	return CIShapeCollection(m_Slide->get_Shapes());
}

CNote CSlide::GetNote() const
{
	if( auto nodesSlideManager = m_Slide->get_NotesSlideManager() ) {
		if( auto nodesSlide = nodesSlideManager->get_NotesSlide() )
			return CNote(nodesSlide);
	}
	
	return CNote();
}

bool CSlide::IsValid() const
{
	return m_Slide != nullptr;
}

CShape CSlide::AsShape()
{
	return CShape(System::dynamic_pointer_cast<Aspose::Slides::IShape>(m_Slide));
}

CBitmap  CSlide::GetBitmap(float scaleX, float scaleY) const
{
	return CBitmap(m_Slide->GetThumbnail(scaleX, scaleY));
}